import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./account/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./account/register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'profile',
    loadChildren: () => import('./account/profile/profile.module').then( m => m.ProfilePageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./features/home/home.module').then( m => m.HomePageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'offers-list',
    loadChildren: () => import('./features/offers-list/offers-list.module').then( m => m.OffersListPageModule)
  },
  {
    path: 'offer-create',
    loadChildren: () => import('./features/offer-create/offer-create.module').then( m => m.OfferCreatePageModule)
  },
  {
    path: 'faq',
    loadChildren: () => import('./features/faq/faq.module').then( m => m.FaqPageModule)
  },
  {
    path: 'offer-details',
    loadChildren: () => import('./features/offer-details/offer-details.module').then( m => m.OfferDetailsPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
