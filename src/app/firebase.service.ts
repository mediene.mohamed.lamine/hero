import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';


@Injectable({
  providedIn: 'root'
})
export class FirebaseService {

  constructor(
    private db: AngularFirestore,
    public auth: AngularFireAuth,
    private router: Router,
    private storage: Storage,
  ) { }

  createUser(user: any) {
    const doc = this.db.collection('users');
    return doc.add(user);
  }

  getUser(uid: string) {
    return this.db.collection('users').ref.where('uid', '==', uid).get()
  }

  logOut() {
    this.auth.signOut().then(() => {
      this.storage.remove('user');
      this.router.navigateByUrl('/login');

    })
  }

  creatOffer(offer: any) {
    const doc = this.db.collection('offers');
    return doc.add(offer);
  }

  getOffers() {
    return this.db.collection('offers').ref.get();
  }

  getOffersByUser(userId: string) {
    return this.db.collection('offers').ref.where('userId', '==', userId).get()
  }

  sendMessage(message: string, id: string) {
    console.log(id);

    this.db.collection('users').doc(id).ref.get().then(async (doc) => {
      let result = doc.data()
      console.log(result);

      // const user: any = results[0]
      // let messages: string[] = user.messages || []
      // messages.push(message)
      // return this.db.doc(user.uid).update({messages :messages})
    })

  }


}
