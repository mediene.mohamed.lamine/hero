import { Component } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { FirebaseService } from './firebase.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'Home', url: '/home', icon: 'home-sharp' },
    { title: 'Offers', url: '/offers-list', icon: 'cube' },
    { title: 'Submit Offer', url: '/offer-create', icon: 'add-circle-sharp' },
    { title: 'My profile', url: '/profile', icon: 'person-sharp' },
    { title: 'Faq', url: '/faq', icon: 'help-sharp' },

  ];
  constructor(
    private storage: Storage,
    private alert: AlertController,
    public firebaseService: FirebaseService,
  ) {}

  async ngOnInit() {
    await this.storage.create();
  }

  async logOut(){
    const alert = await this.alert.create({
      header: 'Confirm',
      message: 'Would you like to disconnect ?',
      cssClass: 'alert',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
        },
        {
          text: 'Oui',
          handler: () => {
            this.firebaseService.logOut();
          }
        }
      ]
    });

    await alert.present();
  }
}
