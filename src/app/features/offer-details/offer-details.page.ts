import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FirebaseService } from 'src/app/firebase.service';

@Component({
  selector: 'app-offer-details',
  templateUrl: './offer-details.page.html',
  styleUrls: ['./offer-details.page.scss'],
})
export class OfferDetailsPage implements OnInit {
  categorySlide = {
    initialSlide: 0,
    slidesPerView: 1,
    centeredSlides: false,
    autoplay: false,
    speed: 500,
    spaceBetween: 7,
  };

  offer: any;
message
  images = [
    'assets/mypic1.jpg',
    'assets/gallery4.jpg',
    'assets/gallery5.jpg',
    'assets/gallery6.jpg',
  ]

  categories = [
    {
      title: 'Electronics',
      icon: 'desktop',
      id: 2
    },
  ]
  constructor(
    private router: Router,
    public firebaseService: FirebaseService,

  ) {
    if (this.router.getCurrentNavigation().extras.state.offer) {
      this.offer = this.router.getCurrentNavigation().extras.state.offer;
      console.log('this.offer: ', this.offer);

    }
  }

  ngOnInit() {
  }

  async sendMessage(uid:string){
   await this.firebaseService.sendMessage(this.message , uid)
  }

}
