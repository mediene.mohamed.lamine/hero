import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-offers-list',
  templateUrl: './offers-list.page.html',
  styleUrls: ['./offers-list.page.scss'],
})
export class OffersListPage implements OnInit {
  offers=[
    {
      id: 1,
      img: 'assets/agent11.jpg',
      title:'XBOX Controller',
      category:'Electronics',
    },
    {
      id: 2,
      img: 'assets/agent2.jpg',
      title:'Karaoke Set',
      category:'Electronics',
    },
    {
      id: 3,
      img: 'assets/agent3.jpg',
      title:'Buffer Jacket',
      category:'Clothing',
    },
    {
      id: 4,
      img: 'assets/agent4.jpg',
      title:'Thermos',
      category:'Kitchenware',
    },
    {
      id: 4,
      img: 'assets/agent5.jpg',
      title:'Car Windshield Sun',
      category:'Clothing',
    },
    {
      id: 4,
      img: 'assets/agent6.jpg',
      title:'Original blue Jays',
      category:'Clothing',
    },
  ]
  constructor() { }

  ngOnInit() {
  }

}
