import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, LoadingController, ToastController } from '@ionic/angular';
import { FirebaseService } from 'src/app/firebase.service';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { AngularFireStorage } from "@angular/fire/compat/storage"
import { finalize } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-offer-create',
  templateUrl: './offer-create.page.html',
  styleUrls: ['./offer-create.page.scss'],
})
export class OfferCreatePage implements OnInit {
  toast: HTMLIonToastElement;
  base64Image: string;
  downloadURL: Observable<string>;
  url: string;
  loading: any;
  user: any
  categories = [
    {
      val: 'Medical Equipment',
      isChecked: false
    },
    {
      val: 'Clothings',
      isChecked: false
    },
    {
      val: 'Kitchenware',
      isChecked: false
    },
    {
      val: 'Electronics',
      isChecked: false
    },
    {
      val: 'Other',
      isChecked: false
    },
  ]
  errorMessages = {
    title: [
      { type: 'required', message: 'This field is required' },
    ],
    type: [
      { type: 'required', message: 'This field is required' },
    ],
    preiority: [
      { type: 'required', message: 'This field is required' },
    ],
    period: [
      { type: 'required', message: 'This field is required' },
    ],
    description: [
      { type: 'required', message: 'This field is required' },
    ],
    address: [
      { type: 'required', message: 'This field is required' },
    ],
    city: [
      { type: 'required', message: 'This field is required' },
    ],
  }
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private camera: Camera,
    private storage: Storage,
    private firestorage: AngularFireStorage,
    public loadingController: LoadingController,
    public toastController: ToastController,
    public firebaseService: FirebaseService,
    public alertController: AlertController,
  ) {
    this.storage.get('user').then(data => {
      this.user = data
    })
  }


  get title() {
    return this.form.get('title');
  }
  get type() {
    return this.form.get('type');
  }
  get priority() {
    return this.form.get('priority');
  }
  get period() {
    return this.form.get('period');
  }
  get description() {
    return this.form.get('description');
  }
  get address() {
    return this.form.get('address');
  }
  get city() {
    return this.form.get('city');
  }
  get category() {
    return this.form.get('category');
  }


  form = this.formBuilder.group({
    title: [
      '',
      [
        Validators.required,
      ]
    ],
    type: [
      '',
      [
        Validators.required,
      ]
    ],
    priority: [
      '',
      [
        Validators.required,
      ]
    ],
    period: [
      '',
      [
        Validators.required,
      ]
    ],
    description: [
      '',
      [
        Validators.required,
      ]
    ],
    address: [
      '',
      [
        Validators.required,
      ]
    ],
    city: [
      '',
      [
        Validators.required,
      ]
    ]

  });

  ngOnInit() {
  }

  async submit() {
    let cat: any[] = [];
    let data: any;
    for (const c of this.categories) {
      if (c.isChecked) {
        cat.push(c.val)
      }
    }
    data = {
       ...this.form.value,
        categories: cat,
         url: this.url ,
         userId: this.user.uid,
         address: this.user.address,
         age: this.user.age,
         name: this.user.name,
         email: this.user.email,
        }
    console.log(data);
    await this.firebaseService.creatOffer(data);
    this.loading.dismiss();
    this.router.navigateByUrl('/home');
  }

  async takePhoto() {
    const options: CameraOptions = {
      quality: 100,
      targetHeight: 500,
      targetWidth: 500,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
    }
    const loading = await this.loadingController.create({
      message: 'Getting Results...',
      translucent: true
    });
    await loading.present();

    this.camera.getPicture(options).then(async (imageData) => {
      this.base64Image = 'data:image/jpeg;base64,' + imageData;
      await loading.dismiss()

    }, async (err) => {
      console.log(err);
      await loading.dismiss()

    });
  }

  async selectPhoto() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM
    }
    const loading = await this.loadingController.create({
      message: 'Loading Image...',
      translucent: true
    });
    await loading.present();

    this.camera.getPicture(options).then(async (imageData) => {
      this.base64Image = 'data:image/jpeg;base64,' + imageData;
      await loading.dismiss()
    }, async (err) => {
      await loading.dismiss()
      console.log(err);
    })
  }

  async presentConfirm() {
    const alert = await this.alertController.create({
      header: 'Select one option ',
      message: 'Take Photo or Select from Galary!',
      buttons: [
        {
          text: 'Camera',
          role: 'camera',
          handler: () => {
            this.takePhoto();
          }
        }, {
          text: 'Gallary',
          role: 'gallary',
          handler: () => {
            this.selectPhoto();
          }
        }
      ]
    });
    await alert.present();
  }

  async presentToast(txt, color) {
    try {
      this.toast.dismiss();
    }
    catch (e) { }
    this.toast = await this.toastController.create({
      message: txt,
      duration: 2500,
      color: color || 'success',
      position: 'top'
    });
    this.toast.present();
  }

  base64ToImage(dataURI) {
    const fileDate = dataURI.split(',');
    const byteString = atob(fileDate[1]);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([arrayBuffer], { type: 'image/png' });
    return blob;
  }

  async upload() {
    this.loading = await this.loadingController.create({
      message: 'Submitting Offer...',
      translucent: true
    });
    await this.loading.present();
    var currentDate = Date.now();
    const file: any = this.base64ToImage(this.base64Image);
    const filePath = `Images/${currentDate}`;
    const fileRef = this.firestorage.ref(filePath);

    const task = this.firestorage.upload(`Images/${currentDate}`, file);
    task.snapshotChanges()
      .pipe(finalize(() => {
        this.downloadURL = fileRef.getDownloadURL();
        this.downloadURL.subscribe(downloadURL => {
          if (downloadURL) {
            this.url = downloadURL;
            this.submit();
          }
        });
      })
      )
    .subscribe(url => {
      if (url) {
        console.log(url);
      }
    });
  }

}
