import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FirebaseService } from 'src/app/firebase.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  categorySlide = {
    initialSlide: 0,
    slidesPerView: 2.7,
    centeredSlides: false,
    autoplay: false,
    speed: 500,
    spaceBetween: 7,
  };

  categories = [
    {
      title: 'Electronics',
      icon: 'desktop',
      id: 2
    },
    {
      title: 'Tools',
      icon: 'hammer',
      id: 2
    },
    {
      title: 'Medical Equipment',
      icon: 'medical',
      id: 2
    },
    {
      title: 'Toys',
      icon: 'football',
      id: 2
    },
  ]

  offers = [
    // {
    //   id: 1,
    //   img: 'assets/property1.jpg',
    //   title: 'Salt & Pepper Shaker',
    //   category: 'Kitchenware',
    // },
    // {
    //   id: 2,
    //   img: 'assets/property3.jpg',
    //   title: 'Offer 2',
    //   category: 'tools',
    // },
    // {
    //   id: 3,
    //   img: 'assets/property2.jpg',
    //   title: 'Offer 3',
    //   category: 'toys',
    // },
    // {
    //   id: 4,
    //   img: 'assets/property4.jpg',
    //   title: 'Offer 4',
    //   category: 'medical',
    // },
  ]
  constructor(
    public firebaseService: FirebaseService,
    private router: Router,
  ) {

  }

  ngOnInit() {
    this.getAllOffers();
  }

  onCategoryClick(category: any) {
  }

  getAllOffers() {
    this.firebaseService.getOffers().then(async (ref) => {
      let results = ref.docs.map(doc => doc.data());
      if (results.length > 0) {
        this.offers = results
      }
    })
  }

  onViewDetails(offer: any) {
    this.router.navigate(['offer-details'], { state: { offer } });
  }

}
