import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { FirebaseService } from 'src/app/firebase.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  user: any
  offers:any
  constructor(
    private storage: Storage,
    public firebaseService: FirebaseService,
    public loadingController: LoadingController,
  ) { }

  ngOnInit() {
    this.storage.get('user').then(data => {
      this.user = data
      this.getUserOffers();
    })
  }

 async getUserOffers() {
    const loading = await this.loadingController.create({
      message: 'Loading data ...',
      translucent: true
    });
    await loading.present();
    this.firebaseService.getOffersByUser(this.user.uid).then(async (ref) => {
      let results = ref.docs.map(doc => doc.data());
      console.log(results);
      await loading.dismiss()

      if (results.length > 0) {
        this.offers = results
      }
      else{
        this.offers = []
      }
    })
  }

}
