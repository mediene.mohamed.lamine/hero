import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { FirebaseService } from 'src/app/firebase.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  toast: HTMLIonToastElement;
  errorMessages = {
    email: [
      { type: 'required', message: 'This field is required' },
      { type: 'pattern', message: 'This field is not valide' },
    ],
    password: [
      { type: 'required', message: 'This field is required' },
      { type: 'minlength', message: 'At least 6 character' }
    ]
  };
  constructor(
    private formBuilder: FormBuilder,
    public auth: AngularFireAuth,
    private router: Router,
    public loadingController: LoadingController,
    public toastController: ToastController,
    private storage: Storage,
    public firebaseService: FirebaseService,
  ) {

  }
  get email() {
    return this.form.get('email');
  }
  get password() {
    return this.form.get('password');
  }

  form = this.formBuilder.group({
    email: [
      '',
      [
        Validators.required,
        Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'),
      ]
    ],
    password: [
      '',
      [
        Validators.required,
        Validators.minLength(6)
      ]
    ]
  });

  ngOnInit() {
  }

  async login() {
    const loading = await this.loadingController.create({
      message: 'Please wait...',
      translucent: true
    });
    await loading.present();
    try {
      const res = await this.auth.signInWithEmailAndPassword(this.form.value.email, this.form.value.password).then(async (data) => {
        await loading.dismiss();
        await this.setUser(data.user.uid)
        this.presentToast('Welcome !', 'success');
      });

    } catch (error) {
      await this.storage.set('user', null);
      await loading.dismiss();
      this.presentToast('Sorry bad credentials !', 'danger');

    }
  }

  async presentToast(txt, color) {
    try {
      this.toast.dismiss();
    }
    catch (e) { }
    this.toast = await this.toastController.create({
      message: txt,
      duration: 2500,
      color: color || 'success',
      position: 'top'
    });
    this.toast.present();
  }

  async setUser(uid: string) {
    this.firebaseService.getUser(uid).then(async (ref) => {
      let results = ref.docs.map(doc => doc.data());
      if (results.length > 0) {
        const user = results[0]
        await this.storage.set('user', user);
        this.router.navigateByUrl('/home');
      }
    })

  }


}
