import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { FirebaseService } from 'src/app/firebase.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  toast: HTMLIonToastElement;
  errorMessages = {
    name: [
      { type: 'required', message: 'This field is required' },
      { type: 'minlength', message: 'At least 3 character' }
    ],
    age: [
      { type: 'required', message: 'This field is required' },
    ],
    address: [
      { type: 'required', message: 'This field is required' },
      { type: 'minlength', message: 'At least 10 character' }
    ],
    email: [
      { type: 'required', message: 'This field is required' },
      { type: 'pattern', message: 'This field is not valide' },
    ],
    password: [
      { type: 'required', message: 'This field is required' },
      { type: 'minlength', message: 'At least 6 character' }
    ]
  }
  constructor(
    private formBuilder: FormBuilder,
    public auth: AngularFireAuth,
    private router: Router,
    private storage: Storage,
    public loadingController: LoadingController,
    public toastController: ToastController,
    public firebaseService: FirebaseService,
  ) { }

  get name() {
    return this.form.get('name');
  }
  get age() {
    return this.form.get('age');
  }
  get address() {
    return this.form.get('address');
  }
  get email() {
    return this.form.get('email');
  }
  get password() {
    return this.form.get('password');
  }

  form = this.formBuilder.group({
    name: [
      '',
      [
        Validators.required,
        Validators.minLength(3),
      ]
    ],
    age: [
      '',
      [
        Validators.required,
      ]
    ],
    address: [
      '',
      [
        Validators.required,
        Validators.minLength(10),
      ]
    ],
    email: [
      '',
      [
        Validators.required,
        Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'),
      ]
    ],
    password: [
      '',
      [
        Validators.required,
        Validators.minLength(6)
      ]
    ]
  });

  ngOnInit() {
  }

  async register() {
    const loading = await this.loadingController.create({
      message: 'Please wait...',
      translucent: true
    });
    await loading.present();
    try {
      const res = await this.auth.createUserWithEmailAndPassword(this.form.value.email, this.form.value.password).then(async (data) => {
        console.log(data);
        this.createUser(data.user.uid);
        await loading.dismiss()
        this.presentToast('Account created !', 'success');
        this.router.navigateByUrl('/home');
      });

    } catch (error) {
      await this.storage.set('user', null);
      await loading.dismiss();
      this.presentToast('Sorry bad credentials !', 'danger');
      console.log(error);
    }


  }
  async presentToast(txt, color) {
    try {
      this.toast.dismiss();
    }
    catch (e) { }
    this.toast = await this.toastController.create({
      message: txt,
      duration: 2500,
      color: color || 'success',
      position: 'top'
    });
    this.toast.present();
  }

  async createUser(uid: string) {
    let user = {
      name: this.form.value.name,
      age: this.form.value.age,
      address: this.form.value.address,
      email: this.form.value.email,
      uid,
    }
    await this.firebaseService.createUser(user);
    await this.storage.set('user', user);
  }

}

