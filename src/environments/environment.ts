// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig :{
    apiKey: "AIzaSyDAZyXxCqb4cngngo0CVagviiKqEMOMV4g",
    authDomain: "hero-f8294.firebaseapp.com",
    projectId: "hero-f8294",
    storageBucket: "hero-f8294.appspot.com",
    messagingSenderId: "758400300671",
    appId: "1:758400300671:web:3f97501aa0707d6cb3f14a"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
